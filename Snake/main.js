const snake = document.querySelector('.snake');
const bait = document.querySelector('.bait');
let space1 = 0;
let space2 = 0;
const SPACE_MOVE = 20;

let interval_move_up = null;
let interval_move_down = null;
let interval_move_left = null;
let interval_move_right = null;
let snakeOffsetX = null;
let snakeOffsetY = null;

let rndNumX = Math.floor(Math.random() * 18)*20;
let rndNumY = Math.floor(Math.random() * 18)*20;

bait.style.marginLeft = rndNumX + "px";
bait.style.marginTop = rndNumY + "px";

let lengthSnake = snake.offsetWidth;

function snakeEat() {
    if(space1 === rndNumY && space2 === rndNumX) {
        rndNumX = Math.floor(Math.random() * 18)*20;
        rndNumY = Math.floor(Math.random() * 18)*20;
        bait.style.marginLeft = rndNumX + "px";
        bait.style.marginTop = rndNumY + "px";
        lengthSnake += 20;
        snake.style.width = lengthSnake + "px";
        console.log(lengthSnake);
    }
}

function move(e) {
    switch(e.key) {
        case "ArrowUp":
            snakeUp();
            break;
        case "ArrowDown":
            snakeDown();
            break;
        case "ArrowLeft":
            snakeLeft();
            break;
        case "ArrowRight":
            snakeRight();
            break;
        default:
            break;
    }
}

function snakeUp(){
    clearInterval(interval_move_left);
    clearInterval(interval_move_right);
    clearInterval(interval_move_down);
    interval_move_up = setInterval(function() {
        space1 -= SPACE_MOVE;
        snake.style.top = space1 + "px";
        snakeEat();
    }, 1000);
}
function snakeDown(){
    clearInterval(interval_move_left);
    clearInterval(interval_move_right);
    clearInterval(interval_move_up);
    interval_move_down = setInterval(function() {
        space1 += SPACE_MOVE;
        snake.style.top = space1 + "px";
        snakeEat();
    }, 1000);
}
function snakeLeft(){
    clearInterval(interval_move_down);
    clearInterval(interval_move_right);
    clearInterval(interval_move_up);
    interval_move_left = setInterval(function() {
        space2 -= SPACE_MOVE;
        snake.style.left = space2 + "px";
        snakeEat();
    }, 1000);
}
function snakeRight(){
    clearInterval(interval_move_left);
    clearInterval(interval_move_down);
    clearInterval(interval_move_up);
    interval_move_right = setInterval(function() {
        space2 += SPACE_MOVE;
        snake.style.left = space2 + "px";
        snakeEat();
    }, 1000);
}

window.addEventListener('keydown', move);