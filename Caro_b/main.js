let countA = 0;
let countB = 0;
let count = 0;
const game = document.querySelector('.game');
const btnOk = document.querySelector('.btn_Ok');
const winner = document.querySelector('.winner');

let data = null;

function showGrid() {
    countA = document.querySelector('[name=countA]').value;
    countB = document.querySelector('[name=countB]').value;
    game.style.grid = `repeat(${countA}, 25px) / auto-flow 25px`;

    for(let i = 0; i < countA*countA; i++) {
        let div = document.createElement("DIV");
        div.classList.add("box");
        game.appendChild(div);
    }

    console.log(countA);
    data = new Array(parseInt(countA));
    for (let i = 0; i < data.length; i++) {
        data[i] = new Array(parseInt(countA));
    }

    const box = document.querySelectorAll('.box');
    box.forEach(e => e.addEventListener('click', tick));
}

function tick() {
    let offsetX = this.offsetLeft/25 - game.offsetLeft/25;
    let offsetY = this.offsetTop/25 - game.offsetTop/25;

    if (this.classList.contains("red") || this.classList.contains("blue")) {
        return;
    }
    count++;

    if (count % 2 === 1) {
        this.innerHTML = "x";
        data[offsetX][offsetY] = "x";
        if (this.classList.contains("red")) {
            this.classList.remove("red");
        }
        this.classList.add("blue");
        let valCur = "x";
        checkRows(offsetX, offsetY, valCur) ? winner.innerHTML = "Chúc mừng " + valCur + " THẮNG!" : "";
        checkCols(offsetX, offsetY, valCur) ? winner.innerHTML = "Chúc mừng " + valCur + " THẮNG!" : "";
        checkDiagonal(offsetX, offsetY, valCur) ? winner.innerHTML = "Chúc mừng " + valCur + " THẮNG!" : "";
    } else {
        this.innerHTML = "o";
        data[offsetX][offsetY] = "o";
        if (this.classList.contains("blue")) {
            this.classList.remove("blue");
        }
        this.classList.add("red");
        let valCur = "o";
        checkRows(offsetX, offsetY, valCur) ? winner.innerHTML = "Chúc mừng " + valCur + " THẮNG!" : "";
        checkCols(offsetX, offsetY, valCur) ? winner.innerHTML = "Chúc mừng " + valCur + " THẮNG!" : "";
        checkDiagonal(offsetX, offsetY, valCur) ? winner.innerHTML = "Chúc mừng " + valCur + " THẮNG!" : "";
    }
}

function checkRows(osX, osY, valCur) {
    let offsetXLast = game.lastElementChild.offsetLeft/25 - game.offsetLeft/25;
    let tx = osX;
    let countRow = 1;
    while(osX - 1 >= 0 && data[osX - 1][osY] === valCur) {
        countRow++;
        osX--;
    }
    osX = tx;
    while(osX + 1 <= offsetXLast && data[osX + 1][osY] === valCur) {
        countRow++;
        osX++;
    }
    if(countRow === parseInt(countB)) return true;
}

function checkCols(osX, osY, valCur) {
    let offsetYLast = game.lastElementChild.offsetTop/25 - game.offsetTop/25;
    let ty = osY;
    let countRow = 1;
    while(osY - 1 >= 0 && data[osX][osY - 1] === valCur) {
        countRow++;
        osY--;
    }
    osY = ty;
    while(osY + 1 <= offsetYLast && data[osX][osY + 1] === valCur) {
        countRow++;
        osY++;
    }
    if(countRow === parseInt(countB)) return true;
}

function checkDiagonal(osX, osY, valCur) {
    let offsetXLast = game.lastElementChild.offsetLeft/25 - game.offsetLeft/25;
    let offsetYLast = game.lastElementChild.offsetTop/25 - game.offsetTop/25;
    let tx = osX;
    let ty = osY;
    let countRow = 1;
    while(osY - 1 >= 0 && osX - 1 >= 0 && data[osX - 1][osY - 1] === valCur) {
        countRow++;
        osY--;
        osX--;
    }
    osX = tx;
    osY = ty;
    while(osY + 1 <= offsetYLast && osX + 1 <= offsetXLast && data[osX + 1][osY + 1] === valCur) {
        countRow++;
        osY++;
        osX++;
    }
    osX = tx;
    osY = ty;
    while(osY - 1 >= 0 && osX + 1 <= offsetXLast && data[osX + 1][osY - 1] === valCur) {
        countRow++;
        osY--;
        osX++;
    }
    osX = tx;
    osY = ty;
    while(osY + 1 <= offsetYLast && osX - 1 >= 0 && data[osX - 1][osY + 1] === valCur) {
        countRow++;
        osY++;
        osX--;
    }
    if(countRow === parseInt(countB)) return true;
}

btnOk.addEventListener('click', showGrid);

