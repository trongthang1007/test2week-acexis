let grid = document.querySelector('.grid');
let btnOk = document.querySelector('.btn_Ok');
let sodong = null;
let socot = null;

let myArr = [];

function  showGrid() {
    sodong = document.querySelector('[name=SD]').value;
    socot = document.querySelector('[name=SC]').value;


    grid.style.grid = `repeat(${sodong}, 50px) / auto-flow 50px`;
    let STT = 1;
    for(let i = 0; i < sodong*socot; i++) {
        let div = document.createElement("DIV");
        div.classList.add("box");
        grid.appendChild(div);
        if(i % sodong === 0) {
            div.innerHTML = STT.toString();
            div.classList.add("header");
            div.style.color = "blue";
            STT++;
        } else {
            let rndNum = Math.floor(Math.random() * 1000) + 1;
            div.innerHTML = rndNum;
            div.classList.add("content");
            myArr.push(rndNum);
        }
    }

    const header = document.querySelectorAll(".header");
    header.forEach(e => e.addEventListener('click', headerClick));
}

function headerClick() {
    let newArr = [];
    let a = [];

    for(let i = 0; i < socot; i++) {
        let a = myArr.slice(i*(sodong-1), (i+1)*(sodong-1));
        newArr.push(myArr.slice(i*(sodong-1), (i+1)*(sodong-1)).sort((a, b) => a - b));
    }

    for (let i = 0; i < newArr.length; i++) {
        a.push(...newArr[i]);
    }

    console.log(a);
    const content = document.querySelectorAll(".content");
    content.forEach((e, cur) => {
        e.innerHTML = a[cur];
    });
}

btnOk.addEventListener('click', showGrid);
