const box = document.querySelectorAll('.box');
const btnReset = document.querySelector('.btn_reset');
const game = document.querySelector('.game');
const winner = document.querySelector('.winner');
let count = 0;

let data = new Array(3);
data[0] = new Array(3);
data[1] = new Array(3);
data[2] = new Array(3);

let offsetXLast = game.lastElementChild.offsetLeft/200 - game.offsetLeft/200;
let offsetYLast = game.lastElementChild.offsetTop/200 - game.offsetTop/200;

function tick(e) {
    let offsetX = this.offsetLeft/200 - game.offsetLeft/200;
    let offsetY = this.offsetTop/200 - game.offsetTop/200;
    if (this.classList.contains("red") || this.classList.contains("blue")) {
        return;
    }
    count++;
    if (count % 2 === 1) {
        this.innerHTML = "x";
        data[offsetX][offsetY] = "x";
        if (this.classList.contains("red")) {
            this.classList.remove("red");
        }
        this.classList.add("blue");
        let valCur = "x";
        checkRows(offsetX, offsetY, valCur) ? winner.innerHTML = "Chúc mừng " + valCur + " THẮNG!" : "";
        checkCols(offsetX, offsetY, valCur) ? winner.innerHTML = "Chúc mừng " + valCur + " THẮNG!" : "";
        checkDiagonal(offsetX, offsetY, valCur) ? winner.innerHTML = "Chúc mừng " + valCur + " THẮNG!" : "";
    } else {
        this.innerHTML = "o";
        data[offsetX][offsetY] = "o";
        if (this.classList.contains("blue")) {
            this.classList.remove("blue");
        }
        this.classList.add("red");
        let valCur = "o";
        checkRows(offsetX, offsetY, valCur) ? winner.innerHTML = "Chúc mừng " + valCur + " THẮNG!" : "";
        checkCols(offsetX, offsetY, valCur) ? winner.innerHTML = "Chúc mừng " + valCur + " THẮNG!" : "";
        checkDiagonal(offsetX, offsetY, valCur) ? winner.innerHTML = "Chúc mừng " + valCur + " THẮNG!" : "";
    }
}

function checkRows(osX, osY, valCur) {
    let tx = osX;
    let countRow = 1;
    while(osX - 1 >= 0 && data[osX - 1][osY] === valCur) {
        countRow++;
        osX--;
    }
    osX = tx;
    while(osX + 1 <= offsetXLast && data[osX + 1][osY] === valCur) {
        countRow++;
        osX++;
    }
    if(countRow === 3) return true;
}

function checkCols(osX, osY, valCur) {
    let ty = osY;
    let countRow = 1;
    while(osY - 1 >= 0 && data[osX][osY - 1] === valCur) {
        countRow++;
        osY--;
    }
    osY = ty;
    while(osY + 1 <= offsetYLast && data[osX][osY + 1] === valCur) {
        countRow++;
        osY++;
    }
    if(countRow === 3) return true;
}

function checkDiagonal(osX, osY, valCur) {
    console.log(osX, osY);
    let tx = osX;
    let ty = osY;
    let countRow = 1;
    while(osY - 1 >= 0 && osX - 1 >= 0 && data[osX - 1][osY - 1] === valCur) {
        countRow++;
        osY--;
        osX--;
    }
    osX = tx;
    osY = ty;
    while(osY + 1 <= offsetYLast && osX + 1 <= offsetXLast && data[osX + 1][osY + 1] === valCur) {
        countRow++;
        osY++;
        osX++;
    }
    osX = tx;
    osY = ty;
    while(osY - 1 >= 0 && osX + 1 <= offsetXLast && data[osX + 1][osY - 1] === valCur) {
        countRow++;
        osY--;
        osX++;
    }
    osX = tx;
    osY = ty;
    while(osY + 1 <= offsetYLast && osX - 1 >= 0 && data[osX - 1][osY + 1] === valCur) {
        countRow++;
        osY++;
        osX--;
    }

    if(countRow === 3) return true;
}


box.forEach(e => e.addEventListener('click', tick));
btnReset.addEventListener('click', () => {
    winner.innerHTML = "";
    box.forEach((e) => {
        e.innerHTML = "";
        e.classList.remove("blue");
        e.classList.remove("red");
    });
    count = 0;
});